#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
APP_HOME=${DIR}/..

JAR=$(find ${APP_HOME} -name "bitcoin*jar")

export SPARK_KAFKA_VERSION=0.10

spark2-submit --master yarn  \
 --deploy-mode cluster \
 --properties-file ${APP_HOME}/cfg/spark.properties \
 --class main.BitcoinBlockAnalysisStreaming ${JAR}