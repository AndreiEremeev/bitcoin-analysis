#!/bin/bash

KEY=$1
HOST=$2
APP_HOME=$3
JAR_LOC=$4
JAR_NAME=$5

scp -i ${KEY} ${JAR_LOC}${JAR_NAME} ${HOST}:${APP_HOME}

ssh -i ${KEY} ${HOST} << EOF
    cd ${APP_HOME}

EOF