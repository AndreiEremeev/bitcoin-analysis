package rpc

import context.{ConfigContext, Env}
import util.autoClose

/**
  * Created by aeremeev on 9/7/17.
  */
object HttpBitcoinClientTest {
  def main(args: Array[String]): Unit = {
    autoClose(new HttpBitcoinClient(ConfigContext(Env.LOCAL), "0000000000000a0125cd12e4b110dc549624206e1383dfe11b78fc8550fcb48d")) { client =>
      client.fetch().foreach(println)
    }
  }
}
