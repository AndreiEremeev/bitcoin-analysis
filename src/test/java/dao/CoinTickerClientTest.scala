package dao

import java.lang.Thread.UncaughtExceptionHandler

import context.{ConfigContext, Env}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import util._

import scala.util.{Failure, Success}

/**
  * Created by aeremeev on 8/3/17.
  */
@RunWith(classOf[JUnitRunner])
class CoinTickerClientTest extends FunSuite {
  test("simple") {
    Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler {
      override def uncaughtException(t: Thread, e: Throwable): Unit = println(t + " " + e)
    })
    autoClose(new CoinTickerClient(ConfigContext(Env.LOCAL))) { client =>
      client.getCurBtcUsd match {
        case Success(_) =>
        case Failure(e) => throw new AssertionError(e)
      }
    }
  }
}
