package dao

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created by aeremeev on 8/3/17.
  */
@RunWith(classOf[JUnitRunner])
class ResponseParserTest extends FunSuite {

  val jsonString: String =
    """
      |[
      |    {
      |        "id": "bitcoin",
      |        "name": "Bitcoin",
      |        "symbol": "BTC",
      |        "rank": "1",
      |        "price_usd": "2757.71",
      |        "price_btc": "1.0",
      |        "24h_volume_usd": "999765000.0",
      |        "market_cap_usd": "45462503976.0",
      |        "available_supply": "16485600.0",
      |        "total_supply": "16485600.0",
      |        "percent_change_1h": "-0.13",
      |        "percent_change_24h": "1.34",
      |        "percent_change_7d": "7.06",
      |        "last_updated": "1501761288"
      |    }
      |]
    """.stripMargin

  test("simple") {
    val rate = ResponseParser.parse(jsonString)
    assert(rate.date.getTime == 1501761288)
    assert(rate.rate == 2757.71)
  }
}
