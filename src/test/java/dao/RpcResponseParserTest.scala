package dao

import org.apache.commons.io.IOUtils
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import rpc.RpcResponseParser

/**
  * Created by aeremeev on 9/7/17.
  */
@RunWith(classOf[JUnitRunner])
class RpcResponseParserTest extends FunSuite {
  test("block parser") {
    for (transfer <- RpcResponseParser.parseBlock(getResource("/block.json"))) {
      println(transfer)
    }
  }

  test("block header") {
    for (transfer <- RpcResponseParser.parseBlockHeader(getResource("/header.json"))) {
      println(transfer)
    }
  }

  def getResource(path: String): String = {
    val is = getClass.getResourceAsStream(path)
    try {
      IOUtils.toString(is, "utf-8")
    } finally {
      is.close()
    }
  }
}
