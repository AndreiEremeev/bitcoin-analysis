package model

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created by aeremeev on 8/2/17.
  */
@RunWith(classOf[JUnitRunner])
class PaymentDestinationTest extends FunSuite {

  test("public key") {
    val address = "4104A39B9E4FBD213EF2"
    test(PaymentDestination(s"bitcoinpubkey_$address"), classOf[BitcoinPubKey], address)
  }

  test("bitcoinaddress") {
    val address = "B76759149150CA58D7"
    test(PaymentDestination(s"bitcoinaddress_$address"), classOf[BitcoinAddress], address)
  }

  test("puzzle") {
    val address = "B76759149150CA58D7"
    test(PaymentDestination(s"puzzle_$address"), classOf[Puzzle], address)
  }

  def test(toTest: PaymentDestination, clazz: Class[_ <: PaymentDestination], address: String) {
    assert(clazz == toTest.getClass)
    assert(toTest.address == address)
  }
}
