package rate

import context.DriverContext
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
  * Created by aeremeev on 8/10/17.
  */
@RunWith(classOf[JUnitRunner])
class RateUploaderTest extends FunSuite {

  test("run") {
    import util._
    autoClose(DriverContext("local")) { ctx =>
      new RateUploader(ctx).run()
    }
  }
}
