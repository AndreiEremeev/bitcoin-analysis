package main

import context.{DriverContext, Env}
import util._

/**
  * Created by aeremeev on 8/15/17.
  */
object BitcoinBlockAnalysisStreamingTest {

  def main(args: Array[String]): Unit = {
    autoClose(DriverContext(Env.LOCAL)) { ctx =>
      new BitcoinBlockAnalysisStreaming(ctx).run()
    }
  }
}