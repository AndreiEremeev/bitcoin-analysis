package main

import context.{DriverContext, WorkerContext}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.slf4j.LoggerFactory
import util.{SerFunctions => ser}

/**
  * Created by aeremeev on 8/4/17.
  */
@RunWith(classOf[JUnitRunner])
class BitcoinBlockAnalysisTest extends FunSuite {
  private[this] val log = LoggerFactory.getLogger(classOf[BitcoinBlockAnalysisTest])

  test("run") {
    new BitcoinBlockAnalysis().run(ser(() => DriverContext("local")), ser(() => WorkerContext("local")))
  }
}
