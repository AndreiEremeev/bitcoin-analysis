package main

import context.DriverContext

/**
  * Created by aeremeev on 8/29/17.
  */
object BitcoinTransferUploaderTest {
  def main(args: Array[String]): Unit = {
    util.autoClose(DriverContext("local")) { ctx => new BitcoinTransferUploader(ctx).run() }
  }
}
