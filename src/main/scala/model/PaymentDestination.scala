package model

import org.slf4j.LoggerFactory

/**
  * Created by aeremeev on 8/2/17.
  */
sealed trait PaymentDestination extends Serializable {
  val address: String

  override def toString: String = getClass.getSimpleName.toLowerCase + "_" + address
}

case class BitcoinAddress(address: String) extends PaymentDestination

case class BitcoinPubKey(address: String) extends PaymentDestination

case class Puzzle(address: String) extends PaymentDestination

object Other extends PaymentDestination {
  override val address: String = ""
}

object PaymentDestination {
  private[this] val log = LoggerFactory.getLogger(PaymentDestination.getClass)

  def apply(str: String): PaymentDestination = {
    if (str == null) return Other
    val index = str.indexOf("_")
    if (index == -1) return Other
    val address = str.substring(index + 1)
    str.substring(0, index) match {
      case "bitcoinpubkey" => BitcoinPubKey(address)
      case "bitcoinaddress" => BitcoinAddress(address)
      case "puzzle" => Puzzle(address)
      case _ =>
        Other
    }
  }

  def apply(t: String, address: String): PaymentDestination = {
    t match {
      case "pubkeyhash" => BitcoinPubKey(address)
      case _ =>
        Other
    }
  }
}