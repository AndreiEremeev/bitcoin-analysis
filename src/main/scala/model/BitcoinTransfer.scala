package model

import java.time.Duration
import java.util.Date

import org.zuinnote.hadoop.bitcoin.format.common.{BitcoinBlock, BitcoinScriptPatternParser, BitcoinUtil}

import scala.collection.JavaConversions._

/**
  * Created by aeremeev on 8/2/17.
  */
case class BitcoinTransfer(
                            txHash: String,
                            destination: PaymentDestination,
                            output: TxOutput,
                            amount: Long,
                            date: Date
                          ) extends Serializable

object BitcoinTransfer {
  def apply(
             txHash: String,
             destination: PaymentDestination,
             output: TxOutput,
             amount: Long,
             seconds: Int
           ): BitcoinTransfer =
    new BitcoinTransfer(txHash, destination, output, amount, new Date(Duration.ofSeconds(seconds).toMillis))

  def apply(
             txHash: ByteArray,
             destination: PaymentDestination,
             output: TxOutput,
             amount: Long,
             seconds: Int
           ): BitcoinTransfer =
    apply(txHash.toString, destination, output, amount, seconds)

  def of(block: BitcoinBlock): Seq[BitcoinTransfer] =
    (for {
      transaction <- block.getTransactions
      txHash = BitcoinUtil.getTransactionHash(transaction)
      (txOutput, outputIndex) <- transaction.getListOfOutputs.zipWithIndex
    } yield
      BitcoinTransfer(
        new ByteArray(txHash),
        PaymentDestination(BitcoinScriptPatternParser.getPaymentDestination(txOutput.getTxOutScript)),
        new TxOutput(new ByteArray(txHash), outputIndex),
        txOutput.getValue,
        block.getTime
      )).filter(filterOthers)

  private val filterOthers: BitcoinTransfer => Boolean =
    _.destination match {
      case Other => false
      case _ => true
    }
}

class TxOutput(val hash: String, val index: Long) extends Serializable {

  def this(hash: ByteArray, index: Long) {
    this(hash.toString, index)
  }

  override def toString: String = s"$hash:$index"
}

class ByteArray(val array: Array[Byte]) extends Serializable {
  override val hashCode: Int = array.deep.hashCode

  override def equals(o: Any): Boolean = o match {
    case o: ByteArray =>
      hashCode == o.hashCode && array.deep == o.array.deep
    case _ => false
  }

  override def toString: String = ByteArray.toHex(array)
}

object ByteArray {
  private[this] val hexArray = "0123456789ABCDEF".toCharArray

  private def toHex(bytes: Array[Byte]): String = {
    val hexChars = new Array[Char](bytes.length * 2)
    for (index <- bytes.indices) {
      val v = bytes(index) & 0xFF
      hexChars(hexChars.length - 2 - 2 * index) = hexArray(v >>> 4)
      hexChars(hexChars.length - 2 - 2 * index + 1) = hexArray(v & 0x0F)
    }
    new String(hexChars)
  }
}
