package main

import java.sql.Timestamp

import context.DriverContext
import kafka.Subscription
import model.{BitcoinTransfer, PaymentDestination}
import org.apache.spark.streaming.Minutes
import org.apache.spark.streaming.kafka010.{CanCommitOffsets, HasOffsetRanges, KafkaUtils, LocationStrategies}

/**
  * Created by aeremeev on 8/4/17.
  */
class BitcoinBlockAnalysisStreaming(val driver: DriverContext) {

  def run(): Unit = {
    val stream = driver.newStreaming(Minutes(1))

    val subscription = Subscription[String, BitcoinTransfer](driver.config.config.getConfig("bitcoin.kafka.consumer"))

    val kafkaStream = KafkaUtils.createDirectStream(
      stream,
      LocationStrategies.PreferConsistent,
      subscription.consumerStrategy
    )

    kafkaStream
      .foreachRDD(rdd => {
        val offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges

        val minMaxDate = rdd.map(r => r.value().date.getTime)
          .map(r => (r, r))
          .fold((Long.MaxValue, Long.MinValue))((d1, d2) => (math.min(d1._1, d2._1), math.max(d1._2, d2._2)))

        val topAddresses = rdd
          .map(r => (r.value().destination, BigInt(r.value().amount)))
          .reduceByKey(_ + _)
          .top(100)(new Ordering[(PaymentDestination, BigInt)] {
            override def compare(x: (PaymentDestination, BigInt), y: (PaymentDestination, BigInt)): Int = x._2.compare(y._2)
          })
          .map { case (destination, amount) => (
            destination.address,
            new Timestamp(minMaxDate._1),
            new Timestamp(minMaxDate._2),
            amount)
          }

        if (topAddresses.nonEmpty) {
          import driver.sparkSession.implicits._
          driver.sparkSession.createDataset[(String, Timestamp, Timestamp, BigInt)](topAddresses.toSeq)
            .write
            .insertInto("HIVE_BITCOIN_ADDRESS_STAT")
        }
        kafkaStream.asInstanceOf[CanCommitOffsets].commitAsync(offsetRanges)
      })

    stream.start()
    stream.awaitTermination()
  }
}

object BitcoinBlockAnalysisStreaming extends App {
  util.autoClose(DriverContext()) { ctx =>
    new BitcoinBlockAnalysisStreaming(ctx).run()
  }
}