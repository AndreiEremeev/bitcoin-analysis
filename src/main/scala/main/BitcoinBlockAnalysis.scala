package main

import java.sql.Timestamp

import context.{DriverContext, WorkerContext}
import dao.BitcoinAddressStat
import model.BitcoinTransfer
import org.apache.spark.sql.expressions.Window
import org.slf4j.LoggerFactory
import util.{SerFunctions => ser, _}

/**
  * Created by aeremeev on 8/4/17.
  */
class BitcoinBlockAnalysis {

  def run(driver: SerFunction0[DriverContext], exec: SerFunction0[WorkerContext]): Unit = {
    autoClose(driver()) { driver =>
      val dao = driver.daoContext

      val spikes = driver.jdbcContext.btcUsdRateDao.spikes()

      val bitcoinBlocksRDD = dao.bitcoinDao.listBlocks()

      import util.RichDate._

      val suspiciousTransfers = bitcoinBlocksRDD
        .flatMap { case (_, block) => BitcoinTransfer.of(block) }
        .flatMap { t =>
          spikes.find(d => d.first.date <= t.date && t.date < d.second.date)
            .map(d => (t, d))
        }

      import driver.sparkSession.implicits._
      import org.apache.spark.sql.functions._

      val addressCol = $"address"
      val startTimeCol = $"startTime"
      val endTimeCol = $"endTime"
      val amountCol = $"amount"
      suspiciousTransfers
        .map { case (b, d) => ((b.destination, d.first.date, d.second.date), BigInt(b.amount)) }
        .reduceByKey(_ + _)
        .map { case ((destination, startTime, endTime), amount) =>
          (destination.address, new Timestamp(startTime.getTime), new Timestamp(endTime.getTime), amount)
        }
        .toDF("address", "startTime", "endTime", "amount")
        .select(
          addressCol,
          startTimeCol,
          endTimeCol,
          amountCol,
          rank().over(Window.partitionBy(startTimeCol, endTimeCol).orderBy(desc("amount"))).alias("rank")
        )
        .where($"rank" <= 25)
        .select(
          addressCol,
          startTimeCol,
          endTimeCol,
          amountCol
        ).orderBy(startTimeCol, amountCol.desc)
        .rdd
        .map(r => new BitcoinAddressStat(
          r.getString(0),
          r.getTimestamp(1),
          r.getTimestamp(2),
          r.getAs(3))
        )
        .repartition(2)
        .foreachPartition(iter => {
          val ex = exec()
          for (coll <- iter.grouped(1000))
            ex.jdbcContext.bitcoinStatDao.writeAddressInfo(coll)
        })
    }
  }
}

object Main {
  private[this] val log = LoggerFactory.getLogger(Main.getClass)

  def main(args: Array[String]): Unit = {
    new BitcoinBlockAnalysis().run(ser(() => DriverContext()), ser(() => WorkerContext()))
  }
}