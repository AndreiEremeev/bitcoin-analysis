package main

import java.nio.file.{Files, Paths, Path => JPath}

import context.WorkerContext
import org.apache.hadoop.fs.{Path, PathFilter}
import org.slf4j.LoggerFactory
import util._

/**
  * Created by aeremeev on 8/8/17.
  */
object BitcoinBlockUploader {
  private[this] val log = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    val ctx = WorkerContext()
    val hdfs = ctx.hdfsContext
    val location = "/bitcoin/blocks"
    val fileSystem = hdfs.getFileSystem
    val hdfsFiles = fileSystem
      .listStatus(new Path(hdfs.at(location)),
        new PathFilter {
          override def accept(path: Path): Boolean = isBitcoinBlock(path.getName)
        })
      .map(s => (s.getPath.getName, s.getLen))
      .toMap

    val isBlock: JPath => Boolean = file => isBitcoinBlock(file.getFileName.toString)
    val checkFile: JPath => Boolean = file => !hdfsFiles.get(file.getFileName.toString).contains(Files.size(file))
    val upload: JPath => Unit = file => {
      val src = new Path(file.toString)
      val dest = new Path(location)
      log.info("uploading file {} to {}", Array(src, dest): _*)
      fileSystem.copyFromLocalFile(false, true, src, dest)
    }

    autoClose(Files.list(Paths.get(ctx.config.config.getString("bitcoin.block.datadir")))) { files =>
      files.parallel
        .filter(isBlock)
        .filter(checkFile)
        .forEach(upload)
    }
  }

  private[this] def isBitcoinBlock(fileName: String) = fileName.matches("blk.*dat")
}
