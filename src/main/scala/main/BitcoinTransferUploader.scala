package main

import java.io.{File, FilenameFilter}

import context.DriverContext
import kafka.Producer
import model.BitcoinTransfer
import org.apache.hadoop.fs.{Path, PathFilter}
import org.slf4j.LoggerFactory
import util._

/**
  * Created by aeremeev on 8/8/17.
  */
class BitcoinTransferUploader(val ctx: DriverContext) {
  private[this] val log = LoggerFactory.getLogger(getClass)

  def run(): Unit = {
    val hdfs = ctx.hdfsContext
    val location = "/bitcoin/blocks"
    val fileSystem = hdfs.getFileSystem
    val hdfsFiles = fileSystem
      .listStatus(new Path(hdfs.at(location)),
        new PathFilter {
          override def accept(path: Path): Boolean = isBitcoinBlock(path.getName)
        })
      .map(s => (s.getPath.getName, s.getLen))
      .toMap

    val config = ctx.config.config
    val localFiles = new File(config.getString("bitcoin.block.datadir"))
      .listFiles(new FilenameFilter {
        override def accept(dir: File, name: String): Boolean = isBitcoinBlock(name)
      })

    if (localFiles == null) throw new IllegalArgumentException

    for (file <- localFiles
      .filter(file => isBitcoinBlock(file.getName))
      .filter(file => !hdfsFiles.get(file.getName).contains(file.length()))
      .sortBy(_.getName)
      .dropRight(1)) {

      autoClose(new Producer[String, BitcoinTransfer](config.getConfig("bitcoin.kafka.producer"))) { p =>
        val messages = ctx.daoContext.bitcoinDao.listBlocks(file.toURI.toString)
          .flatMap { case (_, block) => BitcoinTransfer.of(block) }
          .collect()
        var cnt = 0
        for (transfer <- messages) {
          p.send(transfer.txHash.toString, transfer)
          cnt += 1
          if (cnt % 10000 == 0 || cnt == messages.length) {
            log.info("sent messages {} of {}", cnt, messages.length)
          }
        }
        log.info("sent messages {}", messages.length)
      }
      val src = new Path(file.toString)
      val dest = new Path(location)
      log.info("uploading file {} to {}", Array(src, dest): _*)
      fileSystem.copyFromLocalFile(false, true, src, dest)
    }
  }

  private[this] def isBitcoinBlock(fileName: String) = fileName.matches("blk.*dat")
}

object BitcoinTransferUploader extends App {
  autoClose(DriverContext()) { ctx => new BitcoinTransferUploader(ctx).run() }
}
