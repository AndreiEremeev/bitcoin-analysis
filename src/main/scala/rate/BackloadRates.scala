package rate

import com.opencsv.CSVParser
import context.WorkerContext
import dao.BtcUsdRate

/**
  * Created by aeremeev on 8/10/17.
  */
object BackloadRates {
  def main(args: Array[String]): Unit = {
    val file = args(0)
    val exec = WorkerContext()
    val parser = new CSVParser()
    exec.jdbcContext.btcUsdRateDao.addRate(
      scala.io.Source.fromFile(file).getLines()
        .map(line => parser.parseLine(line))
        .map(s => BtcUsdRate(java.lang.Double.parseDouble(s(1)), s(0)))
        .toArray)
  }
}
