package rate

import context.DriverContext
import dao.CoinTickerClient
import util._

import scala.util.{Failure, Success}

/**
  * Created by aeremeev on 8/3/17.
  */
class RateUploader(val ctx: DriverContext) {
  def run(): Unit = {
    autoClose(new CoinTickerClient(ctx.config)) { client =>
      client.getCurBtcUsd match {
        case Success(rate) => ctx.jdbcContext.btcUsdRateDao.addRate(rate)
        case Failure(e) => println("failure: " + e)
      }
    }
  }
}

object Main {
  def main(args: Array[String]): Unit = {
    autoClose(DriverContext()) { a =>
      new RateUploader(a).run()
    }
  }
}