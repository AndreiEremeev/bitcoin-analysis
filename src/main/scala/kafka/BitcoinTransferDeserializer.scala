package kafka

import java.io.{ByteArrayInputStream, InputStream, ObjectInputStream}
import java.util

import model.BitcoinTransfer
import org.apache.kafka.common.serialization.Deserializer

/**
  * Created by aeremeev on 8/29/17.
  */
class BitcoinTransferDeserializer extends Deserializer[BitcoinTransfer] {
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {

  }

  override def deserialize(topic: String, data: Array[Byte]): BitcoinTransfer = {
    if (data == null || data.length == 0) return null
    new ObjectInputStream(new ByteArrayInputStream(data)).readObject().asInstanceOf[BitcoinTransfer]
  }

  override def close(): Unit = {

  }
}

