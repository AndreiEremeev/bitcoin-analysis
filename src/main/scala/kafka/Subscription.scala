package kafka

import java.util.Collections

import com.typesafe.config.Config
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, ConsumerStrategy}

/**
  * Created by aeremeev on 8/29/17.
  */
class Subscription[K, V](val topic: String, val props: Map[String, Object]) {
  import scala.collection.JavaConverters._
  val consumerStrategy: ConsumerStrategy[K, V] = ConsumerStrategies.Subscribe[K, V](
    Collections.singletonList(topic), props.asJava)
}

object Subscription {
  def apply[K, V](conf: Config): Subscription[K, V] = {
    new Subscription[K, V](conf.getString("topic-name"),
      Map[String, Object](
        ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> conf.getString("brokers"),
        ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> conf.getString("key.deserializer"),
        ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> conf.getString("value.deserializer"),
        ConsumerConfig.GROUP_ID_CONFIG -> conf.getString("group-id"),
        ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "earliest",
        ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG -> (false: java.lang.Boolean)
      ))
  }
}
