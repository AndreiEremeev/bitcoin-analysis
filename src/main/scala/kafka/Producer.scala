package kafka

import com.typesafe.config.Config
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

import scala.collection.JavaConverters._

/**
  * Created by aeremeev on 8/28/17.
  */
class Producer[K, V](val conf: Config) extends AutoCloseable {

  private[this] val props = Map[String, Object](
    ProducerConfig.BOOTSTRAP_SERVERS_CONFIG -> conf.getString("brokers"),
    ProducerConfig.CLIENT_ID_CONFIG -> conf.getString("group-id"),
    ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG -> conf.getString("key.serializer"),
    ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG -> conf.getString("value.serializer")
  )

  private[this] val topic = conf.getString("topic-name")
  private[this] val producer = new KafkaProducer[K, V](props.asJava)

  def send(key: K, value: V): Unit = {
    producer.send(new ProducerRecord[K, V](topic, key, value))
  }

  def flush(): Unit = producer.flush()

  override def close(): Unit = producer.close()
}
