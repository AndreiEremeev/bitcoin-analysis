package kafka

import java.io.{ByteArrayOutputStream, ObjectOutputStream}
import java.util

import model.BitcoinTransfer
import org.apache.kafka.common.serialization.Serializer

/**
  * Created by aeremeev on 8/29/17.
  */
class BitcoinTransferSerializer extends Serializer[BitcoinTransfer] {
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {

  }

  override def serialize(topic: String, data: BitcoinTransfer): Array[Byte] = {
    if (data == null) return null
    val baos = new ByteArrayOutputStream()
    new ObjectOutputStream(baos).writeObject(data)
    baos.toByteArray
  }

  override def close(): Unit = {

  }
}
