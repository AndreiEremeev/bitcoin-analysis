package util

import java.util.Date

import scala.language.implicitConversions

/**
  * Created by aeremeev on 8/4/17.
  */
class RichDate(val date: Date) extends AnyVal with Ordered[RichDate] {
  override def compare(that: RichDate): Int = date.compareTo(that.date)
}

object RichDate {
  implicit def toRichDate(date: Date): RichDate = new RichDate(date)
}
