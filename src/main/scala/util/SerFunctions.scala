package util

/**
  * Created by aeremeev on 8/14/17.
  */
object SerFunctions {
  def apply[R](f: () => R): SerFunction0[R] = new SerFunction0[R] {
    override def apply(): R = f()
  }
}

abstract class SerFunction0[+R] extends (() => R) with Serializable {

}
