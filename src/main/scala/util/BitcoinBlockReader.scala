package util

import java.io.InputStream

import org.zuinnote.hadoop.bitcoin.format.common.{BitcoinBlock, BitcoinBlockReader => HBitcoinBlockReader}
import org.zuinnote.hadoop.bitcoin.format.exception.BitcoinBlockReadException

/**
  * Created by aeremeev on 8/29/17.
  */
class BitcoinBlockReader(private[this] val is: InputStream) extends AutoCloseable {
  private[this] val blockReader = new HBitcoinBlockReader(
    is, Integer.MAX_VALUE, 4096,
    Array("F9BEB4D9".getBytes("utf-8")),
    false)

  def iterator(): Iterator[BitcoinBlock] = new Iterator[BitcoinBlock] {
    var currentBlock: BitcoinBlock = _

    override def hasNext: Boolean = {
      if (currentBlock != null) return true
      try {
//        blockReader.seekBlockStart()
        currentBlock = blockReader.readBlock()
        true
      } catch {
        case e: BitcoinBlockReadException =>
          e.printStackTrace()
          false
      }
    }

    override def next(): BitcoinBlock = {
      if (!hasNext) throw new NoSuchElementException()
      val res = currentBlock
      currentBlock = null
      res
    }
  }

  override def close(): Unit = blockReader.close()
}
