import java.util.function.{Consumer, Predicate, Function => JFunction}

import scala.language.implicitConversions
import scala.util.{Failure, Success, Try}

/**
  * Created by aeremeev on 8/1/17.
  */
package object util {
  def autoClose[A <: AutoCloseable, B](resource: A)(action: A => B): B =
    cleanify(resource)(_.close())(action).get

  def cleanify[A, B](resource: A)(cleanup: A => Unit)(action: A => B): Try[B] = {
    var exc: Exception = null
    try {
      Success(action(resource))
    } catch {
      case e: Exception =>
        exc = e
        Failure(exc)
    } finally {
      try {
        if (resource != null) {
          cleanup(resource)
        }
      } catch {
        case e: Exception =>
          if (exc != null) {
            exc.addSuppressed(e)
          } else {
            exc = e
          }
      }
      Failure(exc)
    }
  }

  implicit def toJavaFunction[U, V](f: U => V): JFunction[U, V] = new JFunction[U, V] {
    override def apply(t: U): V = f(t)
  }

  implicit def fromJavaFunction[U, V](f: JFunction[U, V]): U => V = f.apply

  implicit def toJavaPredicate[U](f: U => Boolean): Predicate[U] = new Predicate[U] {
    override def test(t: U): Boolean = f(t)
  }

  implicit def toJavaConsumer[U](f: U => Unit): Consumer[U] = new Consumer[U] {
    override def accept(t: U): Unit = f(t)
  }
}