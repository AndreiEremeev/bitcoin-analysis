package spark

import context.{ConfigContext, Env}
import model.BitcoinTransfer
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver.Receiver
import rpc.HttpBitcoinClient

/**
  * Created by aeremeev on 9/8/17.
  */
class HttpReceiver(override val storageLevel: StorageLevel, private[this] val env: Env)
  extends Receiver[BitcoinTransfer](storageLevel) {

  @transient private[this] lazy val config = ConfigContext(env)

  private[this] val hash = "0000000000000a0125cd12e4b110dc549624206e1383dfe11b78fc8550fcb48d"

  override def onStart(): Unit =
    new Thread(new Runnable {
      override def run(): Unit = process()
    }).start()

  override def onStop(): Unit = {}

  private[this] def process() = {
    val httpClient = new HttpBitcoinClient(config, hash)
    try {
      while (!isStopped()) {
        httpClient.fetch()
          .foreach(store)

        httpClient.ack()
      }
      restart("restart")
    } catch {
      case e: Throwable =>
        e.printStackTrace()
        restart("recover after error", e)
    } finally {
      httpClient.close()
    }
  }
}
