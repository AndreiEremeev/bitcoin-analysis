package dao

import context.HdfsContext
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
  * Created by aeremeev on 8/15/17.
  */
class BitcoinBlockCounterDao(
                              private[this] val sparkSession: SparkSession,
                              private[this] val hdfs: HdfsContext
                            ) {

  import sparkSession.implicits._

  private[this] val location = "/bitcoin/meta/block-counters"

  def getLastBitcoinFile: Option[BitcoinBlockCounter] = sparkSession.read
    .schema(
      StructType(
        Array(
          StructField("fileName", StringType),
          StructField("size", LongType),
          StructField("lastUpdate", LongType)
        )
      )
    )
    .csv(hdfs.at(location))
    .as[BitcoinBlockCounter]
    .rdd
    .top(1)
    .headOption

  def setLastBitcoinFile(b: BitcoinBlockCounter): Unit = {
    sparkSession.createDataset(Array(b))
      .write
      .mode(SaveMode.Append)
      .csv(hdfs.at(location))
  }
}

case class BitcoinBlockCounter(fileName: String, size: Long, lastUpdate: Long) extends Serializable

object BitcoinBlockCounter {
  implicit val ordering: Ordering[BitcoinBlockCounter] = new Ordering[BitcoinBlockCounter] {
    override def compare(x: BitcoinBlockCounter, y: BitcoinBlockCounter): Int = {
      val cmp = x.fileName.compareTo(y.fileName)
      if (cmp != 0) cmp else x.lastUpdate.compareTo(y.lastUpdate)
    }
  }
}