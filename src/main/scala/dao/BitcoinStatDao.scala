package dao

import java.sql.Timestamp

import util._

/**
  * Created by aeremeev on 8/14/17.
  */
class BitcoinStatDao(private[this] val connectionProvider: ConnectionProvider) {
  def writeAddressInfo(stats: Seq[BitcoinAddressStat]): Unit = {
    autoClose(connectionProvider.openConnection) { connection =>
      autoClose(connection.prepareStatement("INSERT INTO T_BITCOIN_ADDRESS_STAT (START_TS, END_TS, ADDRESS_C, AMOUNT_D) VALUES (?, ?, ?, ?)")) { statement =>
        for (e <- stats) {
          statement.setTimestamp(1, e.startTime)
          statement.setTimestamp(2, e.endTime)
          statement.setString(3, e.address)
          statement.setBigDecimal(4, e.amount)
          statement.addBatch()
        }
        statement.executeBatch()
      }
      connection.commit()
    }
  }
}

class BitcoinAddressStat(
                          val address: String,
                          val startTime: Timestamp,
                          val endTime: Timestamp,
                          val amount: java.math.BigDecimal
                        ) extends Serializable
