package dao

import java.sql.Connection

import context.ConfigContext
import org.apache.commons.dbcp2.BasicDataSource

/**
  * Created by aeremeev on 12.08.17.
  */
class JdbcContext(private[this] val config: ConfigContext) {
  val connectionProvider: ConnectionProvider = new ConnectionProviderImpl(config)

  val btcUsdRateDao: BtcUsdRateDao = new BtcUsdRateDaoImpl(connectionProvider, config)

  val bitcoinStatDao: BitcoinStatDao = new BitcoinStatDao(connectionProvider)
}

trait ConnectionProvider {
  def openConnection: Connection
}

private class ConnectionProviderImpl(val config: ConfigContext) extends ConnectionProvider {
  private lazy val ds = new BasicDataSource

  ds.setDriverClassName(config.config.getString("database.driver"))
  ds.setUsername(config.config.getString("database.username"))
  ds.setPassword(config.config.getString("database.password"))
  ds.setUrl(config.config.getString("database.url"))
  ds.setDefaultAutoCommit(false)
  ds.setMaxTotal(30)
  ds.setMaxOpenPreparedStatements(180)

  override def openConnection: Connection = ds.getConnection
}
