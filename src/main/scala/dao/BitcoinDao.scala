package dao

import context.ConfigContext
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.io.BytesWritable
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.zuinnote.hadoop.bitcoin.format.common.BitcoinBlock
import org.zuinnote.hadoop.bitcoin.format.mapreduce.BitcoinBlockFileInputFormat

/**
  * Created by aeremeev on 7/28/17.
  */
class BitcoinDao(val sparkContext: SparkContext, private[this] val config: ConfigContext) {
  private[this] val hadoopConf: Configuration = new Configuration()
  config.asMap("bitcoin.configuration", classOf[String])
    .foreach { case (key, value) => hadoopConf.set(key, value) }

  val bitcoinBlockLocation: String = config.config.getString("bitcoin.block.location")
  // TODO refactor this
  private[this] val bitcoinBlockLocationUrl: String = config.config.getString("bitcoin.block.locationUrl")

  def listBlocks(): RDD[(BytesWritable, BitcoinBlock)] = listBlocks(bitcoinBlockLocationUrl)

  def listBlocks(path: String): RDD[(BytesWritable, BitcoinBlock)] =
    sparkContext.newAPIHadoopFile(
      path, classOf[BitcoinBlockFileInputFormat], classOf[BytesWritable], classOf[BitcoinBlock], hadoopConf
    )
}
