package dao

import java.io.IOException
import java.lang.{Double, Long}
import java.time.Duration

import scala.util.parsing.json.JSON

/**
  * Created by aeremeev on 8/3/17.
  */
object ResponseParser {
  def parse(input: String): BtcUsdRate = {
    JSON.parseFull(input) match {
      case Some(List(R(rate))) =>
        val S(price) = rate("price_usd")
        val S(date) = rate("last_updated")
        BtcUsdRate(Double.parseDouble(price), Duration.ofSeconds(Long.parseLong(date)))
      case _ => throw new IOException("cannot parse response: " + input)
    }
  }

  private[this] class CC[T] {
    def unapply(a: Any): Option[T] = Some(a.asInstanceOf[T])
  }

  private[this] object R extends CC[Map[String, Any]]

  private[this] object S extends CC[String]
}