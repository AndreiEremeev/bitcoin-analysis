package dao

import context._
import org.apache.spark.sql.SparkSession

/**
  * Created by aeremeev on 8/3/17.
  */
class DaoContext(
                  private[this] val sc: SparkSession,
                  private[this] val hdfs: HdfsContext,
                  private[this] val config: ConfigContext
                ) {

  val bitcoinDao: BitcoinDao = new BitcoinDao(sc.sparkContext, config)

  val bitcoinBlockCounters: BitcoinBlockCounterDao = new BitcoinBlockCounterDao(sc, hdfs)
}