package dao

import java.sql.Timestamp
import java.time.format.DateTimeFormatter
import java.time.{Duration, Instant, LocalDateTime, ZoneId}
import java.util.Date

import context.ConfigContext
import util._

/**
  * Created by aeremeev on 8/3/17.
  */
trait BtcUsdRateDao {
  def intervals(): Seq[RateDiff]

  def spikes(): Seq[RateDiff]

  def spikes(threshold: Double): Seq[RateDiff]

  final def addRate(rate: BtcUsdRate): Unit = {
    addRate(Array(rate))
  }

  def addRate(rate: Seq[BtcUsdRate]): Unit

  def listRates(): Seq[BtcUsdRate]
}

class BtcUsdRateDaoImpl(
                         private[this] val connectionProvider: ConnectionProvider,
                         private[this] val config: ConfigContext) extends BtcUsdRateDao {

  override def intervals(): Seq[RateDiff] = {
    val rates = listRates()
    if (rates.isEmpty) List.empty
    else rates.zip(rates.tail)
      .map { case (first, second) => new RateDiff(first, second) }
  }

  override def spikes(): Seq[RateDiff] = spikes(config.config.getDouble("rate.spike-threshold"))

  override def spikes(threshold: Double): Seq[RateDiff] = intervals().filter(i => math.abs(i.diff) >= threshold)

  override def addRate(rates: Seq[BtcUsdRate]): Unit = {
    autoClose(connectionProvider.openConnection) { connection =>
      autoClose(connection.prepareStatement("INSERT INTO T_RATE(TIME_TS, CURRENCY1_C, CURRENCY2_C, RATE_D) VALUES (?, ?, ?, ?)")) { statement =>
        for (rate <- rates) {
          statement.setTimestamp(1, new Timestamp(rate.date.getTime))
          statement.setString(2, "BTC")
          statement.setString(3, "USD")
          statement.setBigDecimal(4, BigDecimal(rate.rate).bigDecimal)
          statement.addBatch()
        }
        statement.executeBatch()
      }
      connection.commit()
    }
  }

  override def listRates(): Seq[BtcUsdRate] = {
    autoClose(connectionProvider.openConnection) { connection =>
      autoClose(connection.prepareStatement("SELECT TIME_TS, CURRENCY1_C, CURRENCY2_C, RATE_D FROM T_RATE WHERE CURRENCY1_C = 'BTC' AND CURRENCY2_C = 'USD'")) { st =>
        autoClose(st.executeQuery()) { resultSet =>
          var rates = List.empty[BtcUsdRate]
          while (resultSet.next())
            rates =
              BtcUsdRate(resultSet.getBigDecimal("RATE_D").doubleValue(), resultSet.getTimestamp("TIME_TS")) :: rates
          rates
            .groupBy(_.date).map(_._2.head)
            .toList
            .sortBy(_.date)
        }
      }
    }
  }
}

class RateDiff(val first: BtcUsdRate, val second: BtcUsdRate) extends Serializable {
  val absoluteDiff: Double = second.rate - first.rate

  val diff: Double = 2 * absoluteDiff / (first.rate + second.rate)

  override def toString: String =
    s"RateDiff(absoluteDiff=$absoluteDiff, diff=$diff, first=$first, second=$second)"
}

class BtcUsdRate(val rate: Double, val date: Date) extends Serializable {
  override def toString: String = s"BtcUsdRate(rate=$rate, date=$date)"
}

object BtcUsdRate {
  private[this] val DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
  private[this] val UTC = ZoneId.of("UTC")

  def apply(rate: Double, date: Duration): BtcUsdRate = apply(rate, new Date(date.toMillis))

  def apply(rate: Double, date: Date): BtcUsdRate = new BtcUsdRate(rate, date)

  def toCsvArray(rate: BtcUsdRate): Array[String] =
    Array(
      DATE_TIME_FORMAT.format(Instant.ofEpochSecond(rate.date.getTime).atZone(UTC)),
      rate.rate.toString
    )

  private[dao] def apply(rate: Double, date: Timestamp): BtcUsdRate = apply(rate, new Date(date.getTime))

  def apply(rate: Double, date: String): BtcUsdRate =
    apply(rate, Duration.ofMillis(LocalDateTime.parse(date, DATE_TIME_FORMAT).atZone(UTC).toInstant.toEpochMilli))
}
