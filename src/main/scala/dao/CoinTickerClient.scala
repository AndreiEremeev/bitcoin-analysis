package dao

import java.io.IOException

import context.ConfigContext
import dao.CoinTickerClient.log
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIBuilder
import org.apache.http.client.{ClientProtocolException, ResponseHandler}
import org.apache.http.impl.client.{CloseableHttpClient, HttpClients}
import org.slf4j.LoggerFactory

import scala.io.Source
import scala.util.Try

/**
  * Created by aeremeev on 8/3/17.
  */
class CoinTickerClient(private[this] val config: ConfigContext) extends AutoCloseable {
  private[this] val httpClient: CloseableHttpClient = HttpClients.createDefault

  def getCurBtcUsd: Try[BtcUsdRate] = {
    val get = new HttpGet(
      new URIBuilder()
        .setScheme("http")
        .setHost("api.coinmarketcap.com")
        .setPath("/v1/ticker/bitcoin")
        .setParameter("convert", "USD")
        .build()
    )
    log.info("downloading {}", get.toString)
    Try(httpClient.execute(get,
      new ResponseHandler[BtcUsdRate] {
        override def handleResponse(response: HttpResponse): BtcUsdRate = {
          val status = response.getStatusLine.getStatusCode
          if (status >= 200 && status < 300) {
            val entity = response.getEntity
            if (entity == null) throw new IOException("empty entity")
            val rate = ResponseParser.parse(Source.fromInputStream(entity.getContent).mkString)
            log.info("parsed {}", rate)
            rate
          } else {
            throw new ClientProtocolException("Unexpected response status: " + status)
          }
        }
      }))
  }

  override def close(): Unit = httpClient.close()
}

object CoinTickerClient {
  private val log = LoggerFactory.getLogger(CoinTickerClient.getClass)
}