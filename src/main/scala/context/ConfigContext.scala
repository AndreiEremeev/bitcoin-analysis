package context

import java.io.File

import com.typesafe.config.{Config, ConfigFactory, ConfigRenderOptions}
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._

/**
  * Created by aeremeev on 8/1/17.
  */
class ConfigContext private(val env: Env) {
  val config: Config =
    Iterator(ConfigContext.mainConfig(env))
      .flatMap(m => Iterator(ConfigContext.overrideConfig(m), m))
      .foldLeft(ConfigFactory.empty())((a, b) => a.withFallback(b))
      .resolve()

  def render(): String = {
    config.root().render(
      ConfigRenderOptions.defaults()
        .setComments(false)
        .setOriginComments(false)
        .setFormatted(true))
  }

  def asMap[T](string: String, clazz: Class[T]): Map[String, T] =
    (for {
      item <- config.getObjectList(string)
      entry <- item.entrySet()
    } yield (entry.getKey, clazz.cast(entry.getValue.unwrapped()))).toMap
}

object ConfigContext extends ContextCache[ConfigContext](env => new ConfigContext(env)) {
  private val log = LoggerFactory.getLogger(ConfigContext.getClass)

  private def overrideConfig(conf: Config): Config = {
    val file = new File(conf.getString("override.cfg"))
    if (!file.exists()) {
      log.warn("override config not exists: {}", file)
      ConfigFactory.empty()
    } else ConfigFactory.parseFile(file)
  }

  private def mainConfig(env: Env): Config = ConfigFactory.load(env.env + ".conf")
}