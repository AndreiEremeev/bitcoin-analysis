package context

import com.typesafe.config.Config

import scala.language.implicitConversions

/**
  * Created by aeremeev on 8/7/17.
  */
class RichConfig(val config: Config) extends AnyVal {
  def getOptString(path: String): Option[String] =
    if (config.hasPath(path)) Some(config.getString(path))
    else None
}

object RichConfig {
  implicit def toRichConfig(date: Config): RichConfig = new RichConfig(date)
}