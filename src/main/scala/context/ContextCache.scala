package context

import java.util.concurrent.{ConcurrentHashMap => JConcurrentHashMap, ConcurrentMap => JConcurrentMap}

import util._
/**
  * Created by aeremeev on 12.08.17.
  */
class ContextCache[U](private[this] val factory: Env => U) {
  private[this] val context: JConcurrentMap[Env, U] = new JConcurrentHashMap

  final def apply(): U = apply(System.getProperty("env"))

  final def apply(e: String): U = apply(Env(e))

  final def apply(e: Env): U = context.computeIfAbsent(e, factory)
}
