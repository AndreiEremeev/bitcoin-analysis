package context

/**
  * Created by aeremeev on 8/1/17.
  */
class Env private (val env: String) extends Serializable

object Env {
  val LOCAL = new Env("local")
  val AWS = new Env("aws")

  def apply(env: String): Env = env match {
    case "local" => LOCAL
    case "aws" => AWS
    case _ => throw new IllegalArgumentException(env)
  }
}
