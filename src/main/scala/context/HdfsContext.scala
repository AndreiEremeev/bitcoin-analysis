package context

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem

/**
  * Created by aeremeev on 8/2/17.
  */
class HdfsContext(private[this] val config: ConfigContext) {

  val nameNodeUrl: String = config.config.getString("hdfs.namenode.url")

  def at(path: String): String = s"$nameNodeUrl$path"

  def getFileSystem: FileSystem = {
    val conf = new Configuration()
    conf.set(FileSystem.FS_DEFAULT_NAME_KEY, nameNodeUrl)
    FileSystem.get(conf)
  }
}
