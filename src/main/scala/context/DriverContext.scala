package context

import context.RichConfig._
import dao.{DaoContext, JdbcContext}
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.{Duration, StreamingContext}

import scala.reflect.ClassTag

/**
  * Created by aeremeev on 7/28/17.
  */
class DriverContext private(val env: Env) extends AutoCloseable {
  val config = ConfigContext(env)

  private[this] val conf = new SparkConf()
    .setAppName(config.config.getString("application.name"))

  config.config.getOptString("spark.master").foreach(conf.setMaster)
  setConf("hive.metastore.uris")

  lazy val sparkSession: SparkSession = SparkSession.builder()
    .config(conf)
    .enableHiveSupport()
    .getOrCreate()

  lazy val hdfsContext: HdfsContext = new HdfsContext(config)

  lazy val daoContext: DaoContext = new DaoContext(sparkSession, hdfsContext, config)
  lazy val jdbcContext: JdbcContext = new JdbcContext(config)
  def newStreaming(batchDuration: Duration): StreamingContext = new StreamingContext(sparkSession.sparkContext, batchDuration)

  def rdd[T: ClassTag](seq: Seq[T]): RDD[T] = sparkSession.sparkContext.makeRDD(seq)

  override def close(): Unit = sparkSession.close()

  private[this] def setConf(key: String) = {
    config.config.getOptString(key).foreach(v => conf.set(key, v))
  }
}

object DriverContext extends ContextCache[DriverContext](env => new DriverContext(env))

class WorkerContext private(val config: ConfigContext) {
  def this(env: Env) {
    this(ConfigContext(env))
  }

  lazy val hdfsContext: HdfsContext = new HdfsContext(config)
  lazy val jdbcContext: JdbcContext = new JdbcContext(config)
}

object WorkerContext extends ContextCache[WorkerContext](env => new WorkerContext(env))