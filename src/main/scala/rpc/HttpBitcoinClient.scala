package rpc

import java.io.IOException
import java.util.concurrent.{Callable, Executors, TimeUnit}

import context.ConfigContext
import model.BitcoinTransfer
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIBuilder
import org.apache.http.client.{ClientProtocolException, ResponseHandler}
import org.apache.http.impl.client.{CloseableHttpClient, DefaultHttpRequestRetryHandler, HttpClientBuilder}
import org.slf4j.LoggerFactory
import rpc.HttpBitcoinClient.log

import scala.concurrent.ExecutionContext
import scala.io.Source

/**
  * Created by aeremeev on 8/3/17.
  */
class HttpBitcoinClient(private[this] val config: ConfigContext, private[this] var position: String) extends AutoCloseable {
  private[this] val httpClient: CloseableHttpClient =
    HttpClientBuilder.create
      .setMaxConnPerRoute(16)
      .setMaxConnTotal(16)
      .setConnectionTimeToLive(1, TimeUnit.MINUTES)
      .setRetryHandler(new DefaultHttpRequestRetryHandler(10, true))
      .build()

  private[this] val count = 4
  implicit private[this] val executionContext = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(32))

  private[this] var lastReadPosition = position

  def currentPosition: String = lastReadPosition

  def ack(): Unit = position = lastReadPosition

  def fetch(): Seq[BitcoinTransfer] = {
    val headers = new HttpGet(
      host
        .setPath(s"/rest/headers/$count/$position.json")
        .build()
    )

    val blockHashes = httpClient.execute(headers,
      new ResponseHandler[Seq[String]] {
        override def handleResponse(response: HttpResponse): Seq[String] = {
          val status = response.getStatusLine.getStatusCode
          if (status >= 200 && status < 300) {
            val entity = response.getEntity
            if (entity == null) throw new IOException("empty entity")
            val headers = RpcResponseParser.parseBlockHeader(Source.fromInputStream(entity.getContent).mkString)
            log.info("fetched {} block headers", headers.length)
            headers
          } else {
            throw new ClientProtocolException("Unexpected response status: " + status)
          }
        }
      }).drop(1)

    blockHashes.lastOption.foreach(last => lastReadPosition = last)

    val tasks = blockHashes.map(hash =>
      new Callable[Seq[BitcoinTransfer]] {
        override def call(): Seq[BitcoinTransfer] = {
          val blockHash = new HttpGet(
            host
              .setPath(s"/rest/block/$hash.json")
              .build()
          )
          httpClient.execute(blockHash,
            new ResponseHandler[Seq[BitcoinTransfer]] {
              override def handleResponse(response: HttpResponse): Seq[BitcoinTransfer] = {
                val status = response.getStatusLine.getStatusCode
                if (status >= 200 && status < 300) {
                  val entity = response.getEntity
                  if (entity == null) throw new IOException("empty entity")
                  val transfers = RpcResponseParser.parseBlock(Source.fromInputStream(entity.getContent).mkString)
                  log.info(s"fetched ${transfers.length} transfers")
                  transfers
                } else {
                  throw new ClientProtocolException("Unexpected response status: " + status)
                }
              }
            })
        }
      })

    import scala.collection.JavaConversions._
    executionContext.invokeAll(tasks)
      .flatMap(_.get)
      .sortBy(_.date)
  }

  private[this] def host = new URIBuilder()
    .setScheme("http")
    .setHost(config.config.getString("http.bitcoin-rpc.host"))
    .setPort(config.config.getInt("http.bitcoin-rpc.port"))

  override def close(): Unit = {
    try {
      httpClient.close()
    } finally {
      executionContext.shutdownNow()
    }
  }
}

object HttpBitcoinClient {
  private val log = LoggerFactory.getLogger(HttpBitcoinClient.getClass)
}