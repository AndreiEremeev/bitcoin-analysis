package rpc

import java.io.IOException

import model.{BitcoinTransfer, PaymentDestination, TxOutput}

import scala.util.parsing.json.JSON

/**
  * Created by aeremeev on 9/6/17.
  */
object RpcResponseParser {
  def parseBlockHeader(input: String): Seq[String] =
    JSON.parseFull(input) match {
      case Some(A(array)) =>
        (for {
          elem <- array
          hash = elem.asInstanceOf[Map[String, Any]]("hash")
          time = elem.asInstanceOf[Map[String, Any]]("time")
        } yield (hash.asInstanceOf[String], time.asInstanceOf[Double]))
          .sortBy(_._2)
          .map(_._1)
      case _ => throw new IOException("cannot parse response: " + input)
    }

  def parseBlock(input: String): Seq[BitcoinTransfer] =
    JSON.parseFull(input) match {
      case Some(B(block)) =>
        block("tx") match {
          case TS(transactions) =>
            val time = block("time").asInstanceOf[Double].toInt
            for {
              T(tx) <- transactions
              txId = tx("txid").asInstanceOf[String]
              outs = tx("vout")
              V(out) <- outs.asInstanceOf[List[Any]]
              value = out("value").asInstanceOf[Double]
              n = out("n").asInstanceOf[Double]
              P(script) = out("scriptPubKey")
              addressType = script("type").asInstanceOf[String]
              addresses = script("addresses").asInstanceOf[List[String]]
            } yield {
              BitcoinTransfer(
                txId,
                PaymentDestination(addressType, addresses.head),
                new TxOutput(txId, n.toLong),
                (value * 10000000).toLong,
                time
              )
            }
          case _ => Seq.empty
        }
      case _ => throw new IOException("cannot parse response: " + input)
    }

  private[this] class CC[T] {
    def unapply(a: Any): Option[T] = Some(a.asInstanceOf[T])
  }

  private[this] object HS extends CC[List[Any]]

  private[this] object B extends CC[Map[String, Any]]

  private[this] object TS extends CC[List[Any]]

  private[this] object T extends CC[Map[String, Any]]

  private[this] object V extends CC[Map[String, Any]]

  private[this] object P extends CC[Map[String, Any]]

  private[this] object A extends CC[List[Any]]

}

