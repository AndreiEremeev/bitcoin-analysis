CREATE TABLE HIVE_BITCOIN_BLOCK_COUNTER (
        FILE_NAME_C STRING,
        SIZE_I BIGINT,
        LAST_UPDATE_I BIGINT
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\""
)
STORED AS TEXTFILE
LOCATION '/bitcoin/meta/block-counters';