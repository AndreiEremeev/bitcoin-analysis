CREATE TABLE HIVE_BITCOIN_ADDRESS_STAT (
    ADDRESS_C   STRING,
    START_TS    TIMESTAMP,
    END_TS      TIMESTAMP,
    AMOUNT_D    BIGINT
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES (
   "separatorChar" = ",",
   "quoteChar"     = "\""
)
STORED AS TEXTFILE
LOCATION '/bitcoin/output/addresses/real-time';